using System;
using Unity.Mathematics;
using UnityEngine;

public class CalcEnergy : MonoBehaviour
{
    public Transform L1;
    public Transform L2;
    public Transform L3;
    public Transform VirtualSource;
    public Transform Listener;
    private float3x3 lX3;
    public AudioSource LS1;
    public AudioSource LS2;
    public AudioSource LS3;

    public void Start()
    {
        var virtualSourcePosition = Listener.position;
        lX3.c0 = L1.position - virtualSourcePosition;
        lX3.c1 = L2.position - virtualSourcePosition;
        lX3.c2 = L3.position - virtualSourcePosition;
    }

    public void Update()
    {
        if (!Input.GetMouseButton(0)) return;
        
        var sourcePos = VirtualSource.position-Listener.position;
        var lInverse = math.inverse(lX3);
        var g = (lInverse.c0 * sourcePos.x) + (lInverse.c1 * sourcePos.y) + (lInverse.c2 * sourcePos.z);
        L1.localScale = new Vector3(1, g[0], 1);
        L2.localScale = new Vector3(1, g[1], 1);
        L3.localScale = new Vector3(1, g[2], 1);
        LS1.volume = g[0];
        LS2.volume = g[1];
        LS3.volume = g[2];
    }
}