using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookTowards : MonoBehaviour
{
    public Transform lookTo;

    [ContextMenu("Look towards")]
    private void LookTo()
    {
        transform.LookAt(lookTo);
    }
}