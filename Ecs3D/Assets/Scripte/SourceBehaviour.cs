using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class SourceBehaviour : MonoBehaviour
{
    public Camera cameraMain;

    public Transform L1;
    public Transform L2;
    public Transform L3;
    private Renderer L1Renderer;
    private Renderer L2Renderer;
    private Renderer L3Renderer;
    public float scale = 1f;
    public Transform VirtualSource;
    public Transform Listener;
    private float3x3 lX3;
    private float3x3 lInverse;

    public Material materialInside;
    public Material materialOutside;
    public Material materialLS;
    private bool _isInside = true;
    
    
    public AudioSource LS1;
    public AudioSource LS2;
    public AudioSource LS3;
    public AudioSource LS1a;
    public AudioSource LS2a;
    public AudioSource LS3a;
    private Renderer virtualSourceRenderer;
    public void Awake()
    {
        var virtualSourcePosition = Listener.position;
        lX3.c0 = L1.position - virtualSourcePosition;
        lX3.c1 = L2.position - virtualSourcePosition;
        lX3.c2 = L3.position - virtualSourcePosition;
        lInverse = math.inverse(lX3);
         virtualSourceRenderer = VirtualSource.gameObject.GetComponent<Renderer>();
         L1Renderer = L1.gameObject.GetComponentInChildren<Renderer>();
         L2Renderer = L2.gameObject.GetComponentInChildren<Renderer>();
         L3Renderer = L3.gameObject.GetComponentInChildren<Renderer>();
         
         var sourcePos = transform.position - Listener.position;
         UpdateLS(sourcePos);

    }

    private void SetAudio(bool status)
    {
        LS1.mute = status;
        LS2.mute = status;
        LS3.mute = status;
        LS1a.mute = status;
        LS2a.mute = status;
        LS3a.mute = status;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            // Debug.Log(mousePosition);
            if (mousePosition.x > 850f && mousePosition.y > 480f)
                return;
            Ray ray = cameraMain.ScreenPointToRay(mousePosition);
            var endPoint = ray.origin + (ray.direction * 100);
            var rayrev = new Ray(endPoint, -ray.direction);
            //Debug.DrawRay(rayrev.origin, rayrev.direction * 100, Color.green, 10);
            RaycastHit hit;
            // Debug.DrawRay(rayrev.origin, rayrev.direction*100);
            if (!Physics.Raycast(rayrev, out hit, 100)) return;
            
            // Debug.DrawLine(ray.origin, hit.point, Color.green);
            var sourcePos = hit.point - Listener.position;
            UpdateLS(sourcePos);
            transform.position = hit.point;
        }
    }

    private void UpdateLS(Vector3 sourcePos)
    {
        var g = (lInverse.c0 * sourcePos.x) + (lInverse.c1 * sourcePos.y) + (lInverse.c2 * sourcePos.z);

        // if (g.x < -.2f || g.y < -.2f || g.z < -.2f) return;


        if (g.x < 0 || g.y < 0 || g.z < 0)
        {
            // is outside
            if (_isInside)
            {
                virtualSourceRenderer.material = materialOutside;
                SetAudio(true);
                _isInside = false;

                L1Renderer.material = g.x < 0 ? materialOutside : materialLS;
                L2Renderer.material = g.y < 0 ? materialOutside : materialLS;
                L3Renderer.material = g.z < 0 ? materialOutside : materialLS;
            }
        }
        else
        {
            // is inside
            if (!_isInside)
            {
                virtualSourceRenderer.material = materialInside;
                SetAudio(false);
                _isInside = true;

                L1Renderer.material = g.x < 0 ? materialOutside : materialLS;
                L2Renderer.material = g.y < 0 ? materialOutside : materialLS;
                L3Renderer.material = g.z < 0 ? materialOutside : materialLS;
            }
        }

        L1.localScale = new Vector3(1, scale * g[0], 1);
        L2.localScale = new Vector3(1, scale * g[1], 1);
        L3.localScale = new Vector3(1, scale * g[2], 1);
        LS1.volume = g[0];
        LS2.volume = g[1];
        LS3.volume = g[2];
        LS1a.volume = g[0];
        LS2a.volume = g[1];
        LS3a.volume = g[2];
    }
}
