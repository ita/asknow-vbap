%%
hrir_db = DAFF( 'C:\Users\anne\Desktop\ProjectSoundInsulation\Sound Insulation\Directivity\Files\ITAKunstkopfAcademicFreeFieldEqualized.v17.ir.daff' );
[ hrir_ls1 ] = hrir_db.nearest_neighbour_record( 21.65598 , -20.82776);
[ hrir_ls2 ] = hrir_db.nearest_neighbour_record( -46.72179 , -23.78444);
[ hrir_ls3 ] = hrir_db.nearest_neighbour_record( -9.682724 , 33.12912);
file ='MR0804_JesperBuhlTrio_Full_cut_window';
% file ='NewsSpeech';
audio = audioread([file '.wav']);
if size(audio,2)>1
    audio=audio(:,1)+audio(:,2);
end

convolved = conv(hrir_ls1(1,:),audio);
convolved(:,2) = conv(hrir_ls1(2,:),audio);
convolved= convolved./ max(max(abs(convolved)));
audiowrite([file '_LS' num2str(1) '.wav'], convolved,44100);

convolved = conv(hrir_ls2(1,:),audio);
convolved(:,2) = conv(hrir_ls2(2,:),audio);
convolved= convolved./ max(max(abs(convolved)));
audiowrite([file '_LS' num2str(2) '.wav'], convolved,44100);

convolved = conv(hrir_ls3(1,:),audio);
convolved(:,2) = conv(hrir_ls3(2,:),audio);
convolved= convolved./ max(max(abs(convolved)));
audiowrite([file '_LS' num2str(3) '.wav'], convolved,44100);


